//
//  NewPlaceViewController.swift
//  MyPlaces
//
//  Created by admin on 17.01.21.
//

import UIKit
import Cosmos

class NewPlaceViewController: UITableViewController {
    
    var currentPlace: Place!
    var changeImage = false
    var currentRaiting = 0.0
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeLocation: UITextField!
    @IBOutlet weak var placeName: UITextField!
    @IBOutlet weak var placeType: UITextField!
    @IBOutlet weak var cosmosView: CosmosView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        //убираем нижние полоски с таблицы
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        self.saveButton.isEnabled = false
        self.placeName.addTarget(self, action: #selector(textFieldChange), for: .editingChanged)
        setupEditScreen()
        cosmosView.didTouchCosmos = { rating in
            self.currentRaiting = rating
        }
    }

   
    
    // MARK: - Table View delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let photoIcon = #imageLiteral(resourceName: "photo")
            let cameraIcon = #imageLiteral(resourceName: "camera")
            
            let actionSheet = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)
            let camera = UIAlertAction(title: "Camera", style: .default) { _ in
                self.chooseImagePicker(source: .camera)
            }
            //меняем стандартные значения расположения текста и иконки
            camera.setValue(cameraIcon, forKey: "image")
            camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            let photo = UIAlertAction(title: "Photo", style: .default) { _ in
                self.chooseImagePicker(source: .photoLibrary)
            }
            photo.setValue(photoIcon, forKey: "image")
            photo.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")

                let cancel = UIAlertAction(title: "Cancel", style: .cancel)
                //
                actionSheet.addAction(camera)
                actionSheet.addAction(photo)
                actionSheet.addAction(cancel)

             present(actionSheet, animated: true, completion: nil)
            
        }else{
            view.endEditing(true)
        }
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier,
              let mapVC = segue.destination as? MapViewController else {return}
        mapVC.incomeSegueIdentifier = identifier
        mapVC.mapViewControllerDelegate = self
        
        if identifier == "showMap"{
            mapVC.place.name = placeName.text!
            mapVC.place.location = placeLocation.text!
            mapVC.place.type = placeType.text!
            mapVC.place.imageData = placeImage.image?.pngData()
        }
        
    }
    
    
    func savePlace(){
        
        let image = changeImage ? placeImage.image : #imageLiteral(resourceName: "imagePlaceholder")
        let imageData = image?.pngData()
        
        let newPlace = Place(name: self.placeName.text!, location: self.placeLocation.text, type: self.placeType.text, imageData: imageData, rating: self.currentRaiting)
        
        if currentPlace != nil{
            try! realm.write{
                currentPlace?.name = newPlace.name
                currentPlace?.location = newPlace.location
                currentPlace?.type = newPlace.type
                currentPlace?.imageData = newPlace.imageData
                currentPlace?.rating = newPlace.rating
            }
            
        }else{
            StorageManager.saveObject(newPlace)

        }
        
    }
    
    private func setupEditScreen(){
        if currentPlace != nil{
            
            self.setupNavigationBar()
            self.changeImage = true
            guard let data = currentPlace?.imageData, let image = UIImage(data: data) else{return}

            self.placeImage.image = image
            self.placeImage.contentMode = .scaleAspectFill
            
            
            self.placeName.text = currentPlace?.name
            self.placeLocation.text = currentPlace?.location
            self.placeType.text = currentPlace?.type
            //устанавливаем количество звезд в отображении
            cosmosView.rating = currentPlace.rating
        }
    }
    
    
    private func setupNavigationBar(){
        if let topItem = navigationController?.navigationBar.topItem{
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        navigationItem.leftBarButtonItem = nil
        title = currentPlace?.name
        saveButton.isEnabled = true
    }
    
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
   dismiss(animated: true, completion: nil)
    
    }
    
}


// MARK: - TextField Delegate
extension NewPlaceViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //отключаем/включаем кнопку Save
    
    @objc private func textFieldChange(){
        if self.placeName.text?.isEmpty == false{
            self.saveButton.isEnabled = true
        }else{
            self.saveButton.isEnabled = true

        }
    }
}


//MARK: - Work with image

extension NewPlaceViewController: UIImagePickerControllerDelegate,  UINavigationControllerDelegate{
    func chooseImagePicker(source: UIImagePickerController.SourceType){
        if UIImagePickerController.isSourceTypeAvailable(source){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //присваем изображение imageOfPlace из выбранного
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.placeImage.image = info[.editedImage] as? UIImage
        self.placeImage.contentMode = .scaleToFill
        self.placeImage.clipsToBounds = true
        self.changeImage = true
        dismiss(animated: true, completion: nil)
    }
}

extension NewPlaceViewController: MapViewControllerDelegate{
    func getAddress(_ address: String?) {
        placeLocation.text = address
    }
    
    
}

